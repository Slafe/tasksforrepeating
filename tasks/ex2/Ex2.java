package ru.iaa.tasks;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Этот класс содержит метод, который увеличивает заданный элемент массива на 10%.
 *
 */
public class Ex2 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите номер элемента массива: ");
        int element = scanner.nextInt();
        double[] massiv = {14, 15, 10, 9, 18};
        increase(massiv, element);
        System.out.println("Массив с изменённым элементом под номером " + element + ": " + Arrays.toString(massiv));
    }

    /**
     * Этот метод увеличивает заданный элемент массива на 10%
     * @param massiv массив
     * @param element заданный элемент
     */
    public static void increase(double[] massiv, int element) {
        for (int i = 0; i < massiv.length; i++) {
            if (i == element) {
                massiv[i] = massiv[i] + massiv[i] * 0.1;
            }
        }
    }
}
