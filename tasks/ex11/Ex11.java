package tasks;

import java.util.Scanner;

/**
 * Этот класс для проверки: является ли
 * строка палиндромом (одинаково читающееся в обоих направлениях).
 *
 */
public class Ex11 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите строку: ");
        String text = scanner.nextLine();
        if (palindrom(text)) {
            System.out.println(text + " - является палиндромом");
        } else {
            System.out.println(text + " - не является палиндромом");
        }

    }

    /**
     * Метод проверяет, является ли строка палиндромом
     *
     * @param text строка
     * @return false, если строка не является палиндромом, иначе true
     */
    public static boolean palindrom(String text) {
        for (int i = 0; i < text.length() / 2; i++) {
            if (text.charAt(i) != text.charAt(text.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }

}
