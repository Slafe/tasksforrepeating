package tasks;

import java.util.Scanner;

/**
 * Эта программа считает количество часов, минут и секунд в введённом количестве суток.
 */
public class Ex12 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите количество суток: ");
        int countOfDays = scanner.nextInt();
        int hours = countOfDays * 24;
        int minutes = hours * 60;
        int seconds = minutes * 60;
        System.out.println("В " + countOfDays + " cутках " + hours + " часа, " + minutes + " минут, " + seconds + " секунд");
    }
}
