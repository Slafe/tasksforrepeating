package tasks;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Этот класс для построчного переноса двумерного массива заполненного случайными числами в одномерный массив.
 *
 */

public class Ex13 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите количество столбцов двумерного массива: ");
        int col = scanner.nextInt();

        int[][] massiv = new int[2][col];

        int[] newMas = new int[massiv.length * massiv[0].length];

        
        randomMas(massiv);
        transfer(massiv,newMas);
        System.out.println(Arrays.toString(newMas));
    }

    /**
     * Метод для построчного переноса двумерного массива заполненного случайными числами в одномерный массив.
     * @param massiv двумерный массив
     * @param newMas одномерный массив
     */

    public static void transfer(int[][] massiv, int[] newMas) {
        int k = 0;
        for (int i = 0; i < massiv.length; i++) {
            for (int j = 0; j < massiv[i].length; j++) {
                newMas[k++] = massiv[i][j];
            }
        }
    }

    /**
     * Метод для заполнения массива случайными числами
     * @param massiv массив
     */

    public static void randomMas(int[][] massiv) {
        for (int i = 0; i < massiv.length; i++) {
            for (int j = 0; j < massiv[i].length; j++) {
                massiv[i][j] = (int) (Math.random() * 100);
                System.out.print(massiv[i][j] + "\t");
            }
            System.out.println();
        }
    }

}
