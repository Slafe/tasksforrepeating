package tasks;
/**
 * Эта программа вычисляет простые числа в пределах от 2 до 100.
 *
 */
public class Ex5 {
    public static void main(String[] args) {
        for (int i = 2; i <= 100; i++) {
            if (isPrimeNumber(i)) {
                System.out.println("Число " + i + " является простым");
            } else {
                System.out.println("Число " + i + " не является простым");
            }
        }
    }

    /**
     * Метод позволяет определить является ли число простым, или нет
     *
     * @param number число
     * @return false, если число не является простым, иначе true
     */
    public static boolean isPrimeNumber(int number) {
        boolean its_prime = true;
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                its_prime = false;
            }
        }
        return its_prime;
    }
}