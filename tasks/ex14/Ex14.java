package tasks;

import java.util.Scanner;

/**
 * Этот класс позволяет определить,
 * является ли символ введенный с клавиатуры цифрой, буквой или знаком пунктуации.
 */
public class Ex14 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите символ: ");
        String symbol = scanner.nextLine();
        char c = symbol.charAt(0);
        if (Character.isDigit(c)) {
            System.out.println("Это цифра");
        }
        if (Character.isLetter(c)) {
            System.out.println("Это буква");
        }
        if (".,:;!?-...".contains(symbol)) {
            System.out.println("Это знак пунктуации");
        }
    }
}
