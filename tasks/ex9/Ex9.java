package tasks;

import java.util.Scanner;

/**
 * Этот класс содержит методы:
 * 1. Метод, который в качестве аргумента получает число и обнуляет столбец
 * прямоугольной матрицы, который соответствует заданному числу.
 * 2. Метод для вывода матрицы на экран
 *
 */
public class Ex9 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] matrix = {
                {10, 13, 41, 51, 73},
                {31, 61, 68, 85, 89},
        };

        System.out.println("Введите номер столбца: ");
        int column = scanner.nextInt();

        toZero(matrix, column);
        conclusion(matrix);

    }

    /**
     * Этот метод обнуляет столбец
     * прямоугольной матрицы, который соответствует заданному числу.
     * @param matrix матрица
     * @param column заданное число
     */
    public static void toZero(int[][] matrix, int column) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (column == j) {
                    matrix[i][j] = 0;
                }
            }
        }
    }

    /**
     * Метод для вывода матрицы
     * @param matrix матрица
     */
    public static void conclusion(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
