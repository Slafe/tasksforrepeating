package tasks;

import java.util.Scanner;


/**
 * Простая игра, основанная на угадывании чисел.
 * Пользователь должен угадать загаданное число введя его в консоль.
 * Если пользователь угадал число, то программа выведет «Вы угадали» и игра закончится,
 * если нет, то пользователь продолжит вводить числа.
 * Вывести «Загаданное число больше»- если пользователь ввел число меньше загаданного,
 * «Загаданное число меньше»- если пользователь ввел число больше загаданного.
 *
 */
public class Ex7 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Ваша задача угадать число.\nВведите диапазон: ");
        int diapazon = scanner.nextInt();
        System.out.println("Введите количество попыток: ");
        int attempt = scanner.nextInt();
        playLevel(diapazon, attempt);
    }

    private static void playLevel(int diapazon, int attempt) {
        int number = (int) (Math.random() * diapazon);
        System.out.println("Угадайте число от 0 до " + diapazon);
        int i = 1;
        while (i <= attempt) {
            int input_number = scanner.nextInt();
            if (input_number == number) {
                System.out.println("Вы угадали.");
                break;
            } else if (i == attempt) {
                System.out.println("Вы проиграли :с");
            } else if (input_number > number) {
                System.out.println("Загаданное число меньше");
            } else if (input_number < number) {
                System.out.println("Загаданное число больше");
            }
            i++;
        }
    }
}
