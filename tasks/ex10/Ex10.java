package tasks;

import java.util.Scanner;

/**
 * Этот класс содержит метод, который проверяет: является ли число палиндромом.
 *
 */
public class Ex10 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число: ");
        int number = scanner.nextInt();
        String str = String.valueOf(number);
        System.out.println(palindrom(str) ? str + " - является палиндромом" : str + " - не является палиндромом");

    }

    /**
     * Метод проверяет, является ли число палиндромом
     *
     * @param str строка
     * @return false, если число не является палиндромом, иначе true
     */
    public static boolean palindrom(String str) {
        for (int i = 0; i < str.length() / 2; i++) {
            if (str.charAt(i) != str.charAt(str.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }

}
