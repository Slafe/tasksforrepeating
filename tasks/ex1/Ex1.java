package tasks;


import java.util.Scanner;

/**
 * Напишите программу, которая считывает символы, пока не встретится точка.
 * Также предусмотрите вывод количества пробелов.
 */

public class Ex1 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите символ: ");
        String symbol = "";
        int count = 0;
        String text = "";
        do {
            symbol = scanner.nextLine();
            text = text + symbol;
            if (symbol.equals(" ")) {
                count++;
            }

        } while (!symbol.equals("."));

        System.out.println("Текст:" + text + "Количество пробелов: " + count);
    }
}
