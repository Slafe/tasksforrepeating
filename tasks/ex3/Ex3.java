package tasks;
import java.util.Scanner;

/**
 * Это класс содержит метод для перевода рублей в евро по заданному курсу.
 *
 */

public class Ex3 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите количество рублей: ");
        double roubles = scanner.nextDouble();
        System.out.println("Введите курс евро: ");
        double courceOfEuro = scanner.nextDouble();
        translation(roubles, courceOfEuro);
    }

    /**
     * Этот метод переводит рубли в евро по заданному курсу
     * @param roubles количество рублей
     * @param courceOfEuro курс евро
     */
    public static void translation(double roubles, double courceOfEuro) {
        double euro = roubles/courceOfEuro;
        System.out.println(roubles + " рублей - это " + euro + " евро");
    }
}
