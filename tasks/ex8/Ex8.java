package tasks;

import java.util.Scanner;

/**
 * Программу проверяет, является ли число типа double целым.
 *
 */

public class Ex8 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число: ");
        double number = scanner.nextDouble();
        if (number / Math.round(number) != 1.0) {
            System.out.println(number + " - не целое");
        } else System.out.println(number + " - целое");
    }
}
