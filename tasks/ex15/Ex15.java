package tasks;

import java.util.Scanner;

/**
 * Класс меняет в квадратной матрице [n][n] столбцы и строки местами.
 */

public class Ex15 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите количество столбцов квадратной матрицы: ");
        int sizeOfMatrix = scanner.nextInt();
        int[][] matrix = new int[sizeOfMatrix][sizeOfMatrix];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j] = (int) (Math.random() * 100);
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[j][i] + "\t");
            }
            System.out.println();
        }

    }
}

