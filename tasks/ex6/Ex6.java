package tasks;

import java.util.Scanner;

/**
 * Программа для вывода таблицы умножения введенного пользователем числа с клавиатуры.
 *
 */

public class Ex6 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число: ");
        int number = scanner.nextInt();
        for (int i = 2; i<=10;i++) {
            System.out.println(number + " * " + i + " = " + number*i);
        }
    }
}
